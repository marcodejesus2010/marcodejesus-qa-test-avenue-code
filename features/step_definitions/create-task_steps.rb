  Given("I as User, load the ToDo App") do
    Common::loadTODOApp
  end
  
  Given("I click on Sign In Link") do
    expect(ToDoApp::HomePage.headers
        .isSignInLinkReady?
        .assertSignInLink).to be true

    ToDoApp::HomePage.headers
        .clickOnSignInLink()
  end
  
  When("I enter my credentials") do
    expect(ToDoApp::LoginPage.signInForm
        .isEmailTextBoxReady?
        .isPasswordTextBoxReady?
        .assertTextBoxes).to be true

    ToDoApp::LoginPage.signInForm
        .enterEmailAddress($email)

    ToDoApp::LoginPage.signInForm
        .enterPassword($password)
  end
  
  When("I click on Sign in button") do
    expect(ToDoApp::LoginPage.signInForm
      .isSignInButtonReady?
      .assertSignInButton).to be true

    ToDoApp::LoginPage.signInForm
      .clickOnSignInButton()
  end
  
  When("I login successfully") do
    expect(ToDoApp::LandingPage.content
      .isLandingPageReady?
      .assertLandingPage).to be true
      
    expect(ToDoApp::LandingPage.content
      .getSignInMessage).to eq("Signed in successfully.")
  end
  
  When("I click on MyTasks link from the Navigation Bar") do
    expect(ToDoApp::HomePage.headers
      .isMyTaskLinkReady?
      .assertMyTasksLink).to be true

  ToDoApp::HomePage.headers
      .clickOnMyTaskLink()
  end

  When(/^I enter a New Task called (.+)$/) do |task_name|
    expect(ToDoApp::TaskPage.create
      .isNewTaskTextBoxReady?
      .isPlusButtonReady?
      .assertNewTaskFields).to be true

    ToDoApp::TaskPage.create.addTaskNameWithTime(task_name)
  end
  
  When("I click on the Plus Button") do
    expect(ToDoApp::TaskPage.create
      .isNewTaskTextBoxReady?
      .isPlusButtonReady?
      .assertNewTaskFields).to be true

    ToDoApp::TaskPage.create.addATaskByClicking()
  end
  
  Then("a New task should be created in the ToDo List") do
    expect(ToDoApp::TaskPage.listTable
      .isToDoTableReady?
      .assertTable).to be true

    expect(ToDoApp::TaskPage.listTable
      .validateNewTaskIsOnTheTable)
      .to eql($task_name)
  end
  
  Then("I logged out from the App") do
    ToDoApp::LandingPage.content.logOutFromToDoApp()
  end

  Then("I validate that link is ready") do 
    expect(ToDoApp::HomePage.headers
      .isMyTaskLinkReady?
      .assertMyTasksLink).to be true
  end

  Then("I should be addressed to Sign In Page") do
    expect(ToDoApp::LoginPage.signInForm
      .isEmailTextBoxReady?
      .isPasswordTextBoxReady?
      .assertTextBoxes).to be true
  end
  
  Then("an informational message should be displayed saying: {string}") do |message|
    expect(ToDoApp::LoginPage.signInForm
      .getSignInMessage).to eq(message)
  end

  Then("I should be addressed to My Tasks Page") do
    expect(ToDoApp::TaskPage.title
      .isTitleReady?
      .assertPageTitle).to be true

    # TODO: Username needs to be assigned to a global variable and consumed from the app
    expect(ToDoApp::TaskPage.title
      .getTitlePage()).to eq("Marco Antonio De Jesus Ciriaco's ToDo List")
  end

  Then("I get Signed in Succesfully message displayed") do 
    expect(ToDoApp::LandingPage.content
      .getSignInMessage).to eq("Signed in successfully.")
  end

  When("I click on Task Green Button") do
    ToDoApp::LandingPage.content
      .clickOnMyTasksGreenButton
  end

  Then("I should be addressed to My Tasks Page with a message like: “Hey Jon, this is your todo list for today:”") do
    expect(ToDoApp::TaskPage.title
      .isTitleReady?
      .assertPageTitle).to be true

          # TODO: Username needs to be assigned to a global variable and consumed from the app
    expect(ToDoApp::TaskPage.title
      .getTitlePage()).to eq("“Hey Marco Antonio De Jesus Ciriaco, this is your todo list for today:”")
  end
  
  When("I enter a New Task with {int} characters {string}") do |int, task_name|
    expect(ToDoApp::TaskPage.create
      .isNewTaskTextBoxReady?
      .isPlusButtonReady?
      .assertNewTaskFields).to be true

      if(task_name.length >= 3 && task_name.length <= 250)
        ToDoApp::TaskPage.create.addTaskName(task_name)  
      else
        expect(fail).to be true
      end
  end
  
  When("I press the Enter key") do
    expect(ToDoApp::TaskPage.create
      .isNewTaskTextBoxReady?
      .isPlusButtonReady?
      .assertNewTaskFields).to be true

    ToDoApp::TaskPage.create.addATaskByPressingEnterKey()
  end
  
  Then("a New task should not be created in the ToDo List") do
    expect(ToDoApp::TaskPage.listTable
      .isToDoTableReady?
      .assertTable).to be true

    expect(ToDoApp::TaskPage.listTable
      .validateNewTaskIsOnTheTable)
      .not_to eql($task_name)
  end
  
  Then("a New task should be created in the ToDo List with only {int} characters") do |int|
    expect(ToDoApp::TaskPage.listTable
      .isToDoTableReady?
      .assertTable).to be true

    expect(ToDoApp::TaskPage.listTable
      .isCharacterLegthHigherThan250?).to be true
  end
  
  When("I give an space in the New Task textbox") do
    expect(ToDoApp::TaskPage.create
      .isNewTaskTextBoxReady?
      .isPlusButtonReady?
      .assertNewTaskFields).to be true

    ToDoApp::TaskPage.create.addASpaceInNewTaskTextbox()
  end
  
  Then("I should get a message with this legend: {string} in a pop up") do |message|
    expect(ToDoApp::TaskPage.create
      .isNewTaskTextBoxReady?
      .isPlusButtonReady?
      .assertNewTaskFields).to be true

    expect(ToDoApp::TaskPage.create.getPopupMessage()).to eql(message)
  end
  
  When("I Verify the Total number of current ToDo Lsit") do
    expect(ToDoApp::TaskPage.listTable
      .areToDosReady?
      .assertToDosRows).to be true

      @initial_task_count = ToDoApp::TaskPage.listTable.getTaskCount
  end

  Then("the total of tasks is the initial plus one more") do
    expect(ToDoApp::TaskPage.listTable
      .areToDosReady?
      .assertToDosRows).to be true

    @last_task_count = ToDoApp::TaskPage.listTable.getTaskCount
    expect(@last_task_count).to eq(@initial_task_count + 1)
  end
