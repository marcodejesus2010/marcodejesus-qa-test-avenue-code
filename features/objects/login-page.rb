module ToDoApp
    class LoginPageObjects
        attr_accessor :email_textbox, :password_textbox

        @email_textbox = "user_email"
        @password_textbox = "user_password"
        @sign_in_button = "commit"
        @sign_in_up_warning_message = "body > div.container > div.alert.alert-warning"

        def self.email_textbox
            @email_textbox
        end

        def self.password_textbox
            @password_textbox
        end

        def self.sign_in_button
            @sign_in_button
        end

        def self.sign_in_up_warning_message
            @sign_in_up_warning_message
        end
    end
end