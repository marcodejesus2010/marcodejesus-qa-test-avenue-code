module ToDoApp
    class HomePageObjects
        attr_accessor :my_tasks_link, :sign_in_link

         @my_tasks_link = "/html/body/div[1]/div[1]/div/div[2]/ul[1]/li[2]/a"
         @sign_in_link = "/html/body/div[1]/div[1]/div/div[2]/ul[2]/li[1]/a"

         def self.my_tasks_link
            @my_tasks_link
         end

         def self.sign_in_link
            @sign_in_link
         end
    end
end