module ToDoApp
    class TaskPageObjects
        attr_accessor :new_task_textbox, :plus_button, :list_table, :first_table_row

        @new_task_textbox= "new_task"
        @plus_button = "body > div.container > div.task_container.ng-scope > div.well > form > div.input-group > span"
        @list_table = "/html/body/div[1]/div[2]/div[2]/div/table"
        @first_table_row = "/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[1]/td[2]/a"
        @title = "/html/body/div[1]/h1"
        @list_column_headers = "/thead"
        @list_todos = "/tbody/tr"

        def self.new_task_textbox
            @new_task_textbox
        end

        def self.plus_button
            @plus_button
        end

        def self.list_table
            @list_table
        end

        def self.first_table_row
            @first_table_row
        end

        def self.title
            @title
        end

        def self.list_column_headers
            @list_column_headers
        end

        def self.list_todos
            @list_todos
        end
    end
end