module ToDoApp
    class LandingPageObjects
        attr_accessor :signin_successful_message

         @signin_successful_message = "body > div.container > div.alert.alert-info"
         @logout_link = "/html/body/div[1]/div[1]/div/div[2]/ul[2]/li[2]/a"
         @my_tasks_green_button = "body > div.container > div.jumbotron.grey-jumbotron > center > a"

         def self.signin_successful_message
            @signin_successful_message
         end

         def self.logout_link
            @logout_link
         end

         def self.my_tasks_green_button
            @my_tasks_green_button
         end
    end
end