module ToDoApp
    class HeadersCommand
        attr_accessor :my_task_link_is_ready, :my_tasks_link, :sign_in_link
                    :sign_in_link_is_ready

        def initialize
            @my_tasks_link = ToDoApp::HomePageObjects.my_tasks_link
            @sign_in_link = ToDoApp::HomePageObjects.sign_in_link
        end

        def isMyTaskLinkReady?
            wait_for_element_to_display("xpath", @my_tasks_link, $explicit_wait_time)
            is_element_enabled("xpath", @my_tasks_link) &&
            is_element_displayed("xpath", @my_tasks_link) ? @my_task_link_is_ready = true : @my_task_link_is_ready = false
            self
        end

        def clickOnMyTaskLink
            click("xpath", @my_tasks_link)
        end

        def isSignInLinkReady?
            wait_for_element_to_display("xpath", @sign_in_link, $explicit_wait_time)
            is_element_enabled("xpath", @sign_in_link) &&
            is_element_displayed("xpath", @sign_in_link) ? @sign_in_link_is_ready = true : @sign_in_link_is_ready = false
            self
        end

        def clickOnSignInLink
            click("xpath", @sign_in_link)
        end

        def assertMyTasksLink
            @my_task_link_is_ready
        end

        def assertSignInLink
            @sign_in_link_is_ready
        end
    end
end