module ToDoApp
    class FormCommand
        attr_accessor :email_textbox_is_ready, :email_textbox, :password_textbox,
        :password_textbox_is_ready, :sign_in_button_ready, :sign_in_up_warning_message

        def initialize
            @email_textbox = ToDoApp::LoginPageObjects.email_textbox
            @password_textbox = ToDoApp::LoginPageObjects.password_textbox
            @sign_in_button = ToDoApp::LoginPageObjects.sign_in_button
            @sign_in_up_warning_message = ToDoApp::LoginPageObjects.sign_in_up_warning_message
        end

        def isEmailTextBoxReady?
            wait_for_element_to_display("id", @email_textbox, $explicit_wait_time)
            is_element_enabled("id", @email_textbox) &&
            is_element_displayed("id", @email_textbox) ? @email_textbox_is_ready = true : @email_textbox_is_ready = false
            self
        end

        def enterEmailAddress(email)
            enter_text("id", email, @email_textbox)
        end

        def isPasswordTextBoxReady?
            wait_for_element_to_display("id", @password_textbox, $explicit_wait_time)
            is_element_enabled("id", @password_textbox) &&
            is_element_displayed("id", @password_textbox) ? @password_textbox_is_ready = true : @password_textbox_is_ready = false
            self
        end

        def enterPassword(password)
            enter_text("id", password, @password_textbox)
        end

        def isSignInButtonReady?
            wait_for_element_to_display("name", @sign_in_button, $explicit_wait_time)
            is_element_enabled("name", @sign_in_button) &&
            is_element_displayed("name", @sign_in_button) ? @sign_in_button_ready = true : @sign_in_button_ready = false
            self
        end

        def clickOnSignInButton
            click("name", @sign_in_button)
        end

        def assertSignInButton
            @sign_in_button_ready
        end

        def assertTextBoxes
            @email_textbox_is_ready && @password_textbox_is_ready
        end

        def getSignInMessage
            get_element_text("css", @sign_in_up_warning_message)
        end
    end
end