module ToDoApp
    class ContentCommand
        attr_accessor :succes_message_ready, :signin_successful_message, :logout_link

        def initialize
            @signin_successful_message = ToDoApp::LandingPageObjects.signin_successful_message
            @logout_link = ToDoApp::LandingPageObjects.logout_link
            @my_tasks_green_button = ToDoApp::LandingPageObjects.my_tasks_green_button
        end

        def isLandingPageReady?
            $session_active = true
            wait_for_element_to_display("css", @signin_successful_message, $explicit_wait_time)
            is_element_enabled("css", @signin_successful_message) &&
            is_element_displayed("css", @signin_successful_message) ? @succes_message_ready = true : @succes_message_ready = false
            self
        end

        def clickOnMyTasksGreenButton
            click("css", @my_tasks_green_button)
        end

        def getSignInMessage
            get_element_text("css", @signin_successful_message)
        end

        def logOutFromToDoApp
            $session_active = false
            click("xpath", @logout_link)
        end

        def assertLandingPage
            @succes_message_ready
        end
    end
end