module ToDoApp
    class TitleContentCommand
        attr_accessor :title_ready, :title

        def initialize
            @title = ToDoApp::TaskPageObjects.title
        end

        def isTitleReady?
            wait_for_element_to_display("xpath", @title, $explicit_wait_time)
            is_element_enabled("xpath", @title) &&
            is_element_displayed("xpath", @title) ? @title_ready = true : @title_ready = false
            self
        end

        def getTitlePage
            get_element_text("xpath", @title)
        end

        def assertPageTitle
            @title_ready
        end
    end
end