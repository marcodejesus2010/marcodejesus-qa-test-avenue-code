module ToDoApp
    class TaskPage
        def self.create
            CreateCommand.new
        end
        def self.listTable
            ListTableCommand.new
        end
        def self.title
            TitleContentCommand.new
        end
    end
end