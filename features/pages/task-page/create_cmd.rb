module ToDoApp
    class CreateCommand
        attr_accessor :new_task_textbox_ready, :plus_button_ready, :new_task_textbox, :plus_button, :task_name
        def initialize
            @new_task_textbox = ToDoApp::TaskPageObjects.new_task_textbox
            @plus_button = ToDoApp::TaskPageObjects.plus_button
        end

        def isNewTaskTextBoxReady?
            wait_for_element_to_display("id", @new_task_textbox, $explicit_wait_time)
            is_element_enabled("id", @new_task_textbox) &&
            is_element_displayed("id", @new_task_textbox) ? @new_task_textbox_ready = true : @new_task_textbox_ready = false
            self
        end

        def isPlusButtonReady?
            wait_for_element_to_display("css", @plus_button, $explicit_wait_time)
            is_element_enabled("css", @plus_button) &&
            is_element_displayed("css", @plus_button) ? @plus_button_ready = true : @plus_button_ready = false
            self
        end

        def addTaskName(task_name)
            $task_name = task_name 
            enter_text("id", $task_name, @new_task_textbox)
        end

        def addTaskNameWithTime(task_name)
            time = ForDates.new
            $task_name = task_name + " " + time.getUniqueDate
            enter_text("id", $task_name, @new_task_textbox)
        end

        def addATaskByClicking()
            click("css", @plus_button)
        end

        def assertNewTaskFields
            @new_task_textbox_ready && @plus_button_ready
        end

        def addATaskByPressingEnterKey()
            ToDoApp::CoreWebdriver.pressEnterOnElement("id", @new_task_textbox)
        end

        def addASpaceInNewTaskTextbox()
            ToDoApp::CoreWebdriver.pressSpaceOnElement("id", @new_task_textbox)
        end

        def getPopupMessage
            get_alert_text
        end
    end
end