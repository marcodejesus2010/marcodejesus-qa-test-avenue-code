module ToDoApp
    class ListTableCommand
        attr_accessor :list_table_ready, :list_table, :first_table_row, :list_todos, :counter,
                    :list_todos_ready

        def initialize
            @list_table = ToDoApp::TaskPageObjects.list_table
            @first_table_row = ToDoApp::TaskPageObjects.first_table_row
            @list_todos = ToDoApp::TaskPageObjects.list_todos
        end

        def isToDoTableReady?
            wait_for_element_to_display("xpath", @list_table, $explicit_wait_time)
            is_element_enabled("xpath", @list_table) &&
            is_element_displayed("xpath", @list_table) ? @list_table_ready = true : @list_table_ready = false
            self
        end

        def validateNewTaskIsOnTheTable
            get_element_text("xpath", @first_table_row)
        end

        def isCharacterLegthHigherThan250?
            task_name = get_element_text("xpath", @first_table_row)

            task_name.length <= 250 ? true: false
        end

        def areToDosReady?
            wait_for_element_to_display("xpath", @list_table + @list_todos, $explicit_wait_time)
            is_element_enabled("xpath", @list_table + @list_todos) &&
            is_element_displayed("xpath", @list_table + @list_todos) ? @list_todos_ready = true : @list_todos_ready = false
            self
        end

        def getTaskCount
            ToDoApp::CoreWebdriver.getTotalNumberOfRows("xpath",  @list_table + @list_todos)
        end

        def assertToDosRows
            @list_todos_ready
        end

        def assertTable
            @list_table_ready
        end
    end
end