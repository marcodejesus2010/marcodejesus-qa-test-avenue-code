module ToDoApp
    class ForDates
        def initialize
            
        end

        def getUniqueDate
            Time.now.strftime('%Y%m%d%H%M%S%L')
        end
    end

    class CoreWebdriver
        # https://www.rubydoc.info/gems/selenium-webdriver/Selenium/WebDriver/Keyboard#send_keys-instance_method
        # https://www.rubydoc.info/gems/selenium-webdriver/Selenium/WebDriver/Keys
        def self.pressEnterOnElement(locator, value)
            element = $driver.find_element(:"#{locator}", value)
            element.send_keys:return
        end

        def self.pressSpaceOnElement(locator, value)
            element = $driver.find_element(:"#{locator}", value)
            element.send_keys:space
        end

        def self.getTotalNumberOfRows(locator, value)
            element = $driver.find_elements(:"#{locator}", value)
            element.size
        end
    end
end