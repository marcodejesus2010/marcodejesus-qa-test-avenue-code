@US1 @RegressionTesting @LoggedUser
Feature: Create Task

    As a ToDo App user
    I should be able to create a task
    So I can manage my tasks

@SmokeTesting
Scenario: Add a new Task with a Logged user
    Given I as User, load the ToDo App
    And I click on Sign In Link
    When I enter my credentials 
    And I click on Sign in button
    And I login successfully 
    When I click on MyTasks link from the Navigation Bar
    And I enter a New Task called "Set the Meeting"
    And I click on the Plus Button
    Then a New task should be created in the ToDo List 
    And I logged out from the App
    
Scenario Outline: Add several Tasks with a Logged user
    Given I as User, load the ToDo App
    And I click on Sign In Link
    When I enter my credentials 
    And I click on Sign in button
    And I login successfully 
    When I click on MyTasks link from the Navigation Bar
    And I enter a New Task called <ToDo>
    And I click on the Plus Button
    Then a New task should be created in the ToDo List 
    And I logged out from the App

    Examples:
    | ToDo |
    | Complete the Test  | 
    | Call your friends |
    | Study for the exam |
    | Get some Juice |

@TC04
Scenario: Validate that a Log-in user is addressed to task page.
    Given I as User, load the ToDo App
    And I click on Sign In Link
    When I enter my credentials 
    And I click on Sign in button
    And I login successfully 
    When I click on MyTasks link from the Navigation Bar
    Then I should be addressed to My Tasks Page
    And I logged out from the App

@TC05
Scenario: Validate that a Log-in user is addressed to task page from Green Button at Home page. 
    Given I as User, load the ToDo App
    And I click on Sign In Link
    When I enter my credentials 
    And I click on Sign in button
    And I login successfully 
    When I click on Task Green Button
    Then I should be addressed to My Tasks Page
    And I logged out from the App

@TC06 @Bug6210 @Bug
Scenario: Validate Name is displayed in the Task page saying the To do list belongs to the logged user. 
    Given I as User, load the ToDo App
    And I click on Sign In Link
    When I enter my credentials 
    And I click on Sign in button
    And I login successfully 
    When I click on MyTasks link from the Navigation Bar
    Then I should be addressed to My Tasks Page with a message like: “Hey Jon, this is your todo list for today:”
    And I logged out from the App

@TC07
Scenario: Validate logged user can add a task by pressing Enter key, with a task name length equal to 3 characters.
    Given I as User, load the ToDo App
    And I click on Sign In Link
    When I enter my credentials 
    And I click on Sign in button
    And I login successfully 
    When I click on MyTasks link from the Navigation Bar
    And I enter a New Task with 3 characters "Set"
    And I press the Enter key
    Then a New task should be created in the ToDo List 
    And I logged out from the App

@TC08
Scenario: Validate logged user can add a task by pressing Enter key, with a task name length greater than 3 character s and less than 250.
    Given I as User, load the ToDo App
    And I click on Sign In Link
    When I enter my credentials 
    And I click on Sign in button
    And I login successfully 
    When I click on MyTasks link from the Navigation Bar
    And I enter a New Task with 3 characters "Task with more than 3 characters"
    And I press the Enter key
    Then a New task should be created in the ToDo List 
    And I logged out from the App

@TC09 @Bug6211 @Bug
Scenario: Validate logged user cannot add a task by pressing Enter key, with a task name length less than 3 characters.
    Given I as User, load the ToDo App
    And I click on Sign In Link
    When I enter my credentials 
    And I click on Sign in button
    And I login successfully 
    When I click on MyTasks link from the Navigation Bar
    And I enter a New Task with 2 characters "Ta"
    And I press the Enter key
    Then a New task should not be created in the ToDo List 
    And I logged out from the App

@TC10
Scenario: Validate logged user can add a task by pressing Enter key, with a task name length of 250 characters.
    Given I as User, load the ToDo App
    And I click on Sign In Link
    When I enter my credentials 
    And I click on Sign in button
    And I login successfully 
    When I click on MyTasks link from the Navigation Bar
    And I enter a New Task with 250 characters "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nisi turpis, pretium at blandit sed, euismod at metus. Duis tempor tellus ut odio facilisis convallis. Suspendisse egestas erat eu ipsum pulvinar placerat. In hendrerit congue sollicitu"
    And I press the Enter key
    Then a New task should be created in the ToDo List 
    And I logged out from the App

@TC11
Scenario: Validate logged user cannot add a task by pressing Enter key, with a task name length greater than 250 characters.
    Given I as User, load the ToDo App
    And I click on Sign In Link
    When I enter my credentials 
    And I click on Sign in button
    And I login successfully 
    When I click on MyTasks link from the Navigation Bar
    And I enter a New Task with 250 characters "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nisi turpis, pretium at blandit sed, euismod at metus. Duis tempor tellus ut odio facilisis convallis. Suspendisse egestas erat eu ipsum pulvinar placerat. In hendrerit congue sollicitu"
    And I press the Enter key
    Then a New task should be created in the ToDo List 
    And I logged out from the App

@TC12
Scenario: Validate logged user can add a task by clicking on Plus Button, with a task name length equal to 3 characters.
    Given I as User, load the ToDo App
    And I click on Sign In Link
    When I enter my credentials 
    And I click on Sign in button
    And I login successfully 
    When I click on MyTasks link from the Navigation Bar
    And I enter a New Task with 250 characters "oks"
    And I click on the Plus Button
    Then a New task should be created in the ToDo List 
    And I logged out from the App

@TC13   
Scenario: Validate logged user can add a task by clicking on Plus Button, with a task name length greater than 3 character s and less than 250.
    Given I as User, load the ToDo App
    And I click on Sign In Link
    When I enter my credentials 
    And I click on Sign in button
    And I login successfully 
    When I click on MyTasks link from the Navigation Bar
    And I enter a New Task with 3 characters "Task with more than 3 characters"
    And I click on the Plus Button
    Then a New task should be created in the ToDo List 
    And I logged out from the App

@TC14 @Bug6211 @Bug
Scenario: Validate logged user cannot add a task by clicking on Plus Button, with a task name length less than 3 characters.
    Given I as User, load the ToDo App
    And I click on Sign In Link
    When I enter my credentials 
    And I click on Sign in button
    And I login successfully 
    When I click on MyTasks link from the Navigation Bar
    And I enter a New Task with 2 characters "Ta"
    And I click on the Plus Button
    Then a New task should not be created in the ToDo List 
    And I logged out from the App

@TC15
Scenario: Validate logged user can add a task by clicking on Plus Button, with a task name length of 250 characters.
    Given I as User, load the ToDo App
    And I click on Sign In Link
    When I enter my credentials 
    And I click on Sign in button
    And I login successfully 
    When I click on MyTasks link from the Navigation Bar
    And I enter a New Task with 250 characters "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nisi turpis, pretium at blandit sed, euismod at metus. Duis tempor tellus ut odio facilisis convallis. Suspendisse egestas erat eu ipsum pulvinar placerat. In hendrerit congue sollicitu"
    And I click on the Plus Button
    Then a New task should be created in the ToDo List 
    And I logged out from the App

@TC16 @Bug6212 @Bug6213 @Bug
Scenario: Validate logged user cannot add a task by clicking on Plus Button, with a task name length greater than 250 characters.
    Given I as User, load the ToDo App
    And I click on Sign In Link
    When I enter my credentials 
    And I click on Sign in button
    And I login successfully 
    When I click on MyTasks link from the Navigation Bar
    And I enter a New Task with 257 characters "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nisi turpis, pretium at blandit sed, euismod at metus. Duis tempor tellus ut odio facilisis convallis. Suspendisse egestas erat eu ipsum pulvinar placerat. In hendrerit congue sollicitudeadsfs"
    And I click on the Plus Button
    Then a New task should be created in the ToDo List with only 250 characters
    And I logged out from the App

@TC17
Scenario: Validate new created task has been added to the Task List and displayed at the top of the table.
    Given I as User, load the ToDo App
    And I click on Sign In Link
    When I enter my credentials 
    And I click on Sign in button
    And I login successfully 
    When I click on MyTasks link from the Navigation Bar
    And I enter a New Task called "Set the Meeting"
    And I click on the Plus Button
    Then a New task should be created in the ToDo List 
    And I logged out from the App

@TC18 @WIP
Scenario: Validate that “Please fill out this field.” informational message is displayed when trying to create a task with no name (no characters in text box) by pressing Enter key.
    Given I as User, load the ToDo App
    And I click on Sign In Link
    When I enter my credentials 
    And I click on Sign in button
    And I login successfully 
    When I click on MyTasks link from the Navigation Bar
    And I press the Enter key
    Then I should get a message with this legend: "Please fill out this field." in a pop up
    Then a New task should not be created in the ToDo List 
    And I logged out from the App

@TC19 @WIP @Bug6214 @Bug
Scenario: Validate that “Please fill out this field.” informational message is displayed when trying to create a task with no name (no characters in text box) by clicking on Plus icon button.
    Given I as User, load the ToDo App
    And I click on Sign In Link
    When I enter my credentials 
    And I click on Sign in button
    And I login successfully 
    When I click on MyTasks link from the Navigation Bar
    And I click on the Plus Button
    Then I should get a message with this legend: "Please fill out this field." in a pop up
    Then a New task should not be created in the ToDo List 
    And I logged out from the App

@TC20 @WIP @Bug6215 @Bug
Scenario: Validate that “Please fill out this field.” informational message is displayed when trying to create a task with an space (no characters in text box) by pressing Enter key.
    Given I as User, load the ToDo App
    And I click on Sign In Link
    When I enter my credentials 
    And I click on Sign in button
    And I login successfully 
    When I click on MyTasks link from the Navigation Bar
    And I enter a New Task with 0 characters " "
    And I press the Enter key
    Then I should get a message with this legend: "Please fill out this field." in a pop up
    Then a New task should not be created in the ToDo List 
    And I logged out from the App

@TC21 @WIP @Bug6214 @Bug
Scenario: Validate that “Please fill out this field.” informational message is displayed when trying to create a task with an space (no characters in text box) by clicking on Plus icon button.
    Given I as User, load the ToDo App
    And I click on Sign In Link
    When I enter my credentials 
    And I click on Sign in button
    And I login successfully 
    When I click on MyTasks link from the Navigation Bar
    And I enter a New Task with 0 characters " "
    And I click on the Plus Button
    Then I should get a message with this legend: "Please fill out this field." in a pop up
    Then a New task should not be created in the ToDo List 
    And I logged out from the App

@TC22 
Scenario: Validate that already created task count remains when creating adding a new task.
    Given I as User, load the ToDo App
    And I click on Sign In Link
    When I enter my credentials 
    And I click on Sign in button
    And I login successfully 
    When I click on MyTasks link from the Navigation Bar
    And I Verify the Total number of current ToDo Lsit
    And I enter a New Task called "Set the Meeting"
    And I click on the Plus Button
    Then a New task should be created in the ToDo List 
    And the total of tasks is the initial plus one more
    And I logged out from the App


@TC23 @Declarative @IsThisForWebDriver @Ignore @LookAndFeel
Scenario: Validate New Task textbox is aligned with Plus button. 
    Given I as User, load the ToDo App
    And I click on Sign In Link
    When I enter my credentials 
    And I click on Sign in button
    And I login successfully 
    When I click on MyTasks link from the Navigation Bar
    Then I should validate the alignment
    And I logged out from the App