@US1 @RegressionTesting @NoLoggedUser
Feature: Create Task

    As a ToDo App user
    I should be able to create a task
    So I can manage my tasks

Background: Background name
    Given I as User, load the ToDo App

@TC01
Scenario: Validate “My Tasks” link is displayed, enabled and clickable at the Navigation Bar section with a Non-Logged In User.

    Then I validate that link is ready
    
@TC02
Scenario: Validate that “You need to sign in or sign up before continuing.” message is displayed when a non-logged in User is clicking on “My Tasks”.
    Given I click on MyTasks link from the Navigation Bar
    Then I should be addressed to Sign In Page
    And an informational message should be displayed saying: "You need to sign in or sign up before continuing."
    
@TC03
Scenario: Validate that “You need to sign in or sign up before continuing.” message is displayed when a non-logged in User is clicking on “My Tasks”.
    Given I click on MyTasks link from the Navigation Bar
    When I should be addressed to Sign In Page
    And an informational message should be displayed saying: "You need to sign in or sign up before continuing."
    And I enter my credentials
    And I click on Sign in button
    Then I should be addressed to My Tasks Page
    And I get Signed in Succesfully message displayed

    
